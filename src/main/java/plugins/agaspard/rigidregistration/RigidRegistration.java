package plugins.agaspard.rigidregistration;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.SwingConstants;
import javax.vecmath.Vector2d;

import edu.emory.mathcs.jtransforms.fft.FloatFFT_2D;
import flanagan.complex.Complex;
import icy.gui.frame.progress.FailedAnnounceFrame;
import icy.image.IcyBufferedImage;
import icy.image.IcyBufferedImageUtil;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;
import icy.util.StringUtil;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzGroup;
import plugins.adufour.ezplug.EzLabel;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStatus;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVar;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarChannel;
import plugins.adufour.ezplug.EzVarEnum;
import plugins.adufour.ezplug.EzVarFrame;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarListener;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.ezplug.EzVarSlice;
import plugins.adufour.vars.lang.VarSequence;


/**
 * Rigid registration for Icy. This plug-in estimates a rotation (using the Log-Polar Transform)
 * and/or a translation (using the Fast Fourier Transform) between two images using spectral
 * correlation (in the Fourier domain). This can be applied to register a chromatic shift (i.e.
 * align 2 channels of a dataset) or a temporal shift (i.e. correct a drift over time)
 * 
 * @author Alexandre Gaspard-Cilia
 * @author Alexandre Dufour
 * @author Jacques (aka Jacques2020 on Image.sc forum)
 */
public class RigidRegistration extends EzPlug implements EzStoppable, Block
{
    private enum RegMode
    {
        Simple, Advanced
    }
    
    private enum RegType
    {
        Chromatic2D("Chromatic shift (2D)"), Temporal2D("Temporal drift (2D)");
        
        private final String method;
        
        RegType(String method)
        {
            this.method = method;
        }
        
        public String toString()
        {
            return method;
        }
    }
    
    /**
     * Enumeration of the various supported resizing policies for rigid registration
     * 
     * @author Alexandre Dufour
     */
    public enum ResizePolicy
    {
        /**
         * The final bounds of the registered sequence will be grown to ensure no information is
         * lost, while missing pixels will be given an arbitrary value (typically 0).
         */
        UNITE_BOUNDS,
        /**
         * The final bounds of the registered sequence will be identical to that of the initial
         * sequence, centered on the reference frame. Out-of-bounds pixels will be lost, while
         * missing pixels will be given an arbitrary value (typically 0).
         */
        PRESERVE_SIZE,
        /**
         * The final bounds of the registered sequence will be the intersection of all translated
         * bounds. Information will be lost, but the resulting sequence will have no missing pixel.
         */
        INTERSECT_BOUNDS;
        
        @Override
        public String toString()
        {
            return super.toString().toLowerCase().replace('_', ' ');
        }
    }

    EzVarEnum<RegMode> mode = new EzVarEnum<RegMode>("Mode", RegMode.values(), RegMode.Simple);
    EzVarEnum<RegType> type = new EzVarEnum<RegType>("Correct", RegType.values());
    
    // simple mode //
    
    EzVarSequence seq = new EzVarSequence("Reference");
    EzVarInteger refC = new EzVarInteger("Ref. channel", 0, 0, 1, 1);
    EzVarInteger refT = new EzVarInteger("Ref. frame", 0, 0, 1, 1);
    EzVarInteger refZ = new EzVarInteger("Ref. slice", 0, 0, 1, 1);
    EzVarSequence applyToSimple = new EzVarSequence("Apply transform to");

    VarSequence seqVar = new VarSequence("Registered sequence", null);
    VarSequence applyToSimpleVar = new VarSequence("Registered apply to sequence", null);

    
    // For temporal registration only
    
    EzVarBoolean regToPrevT = new EzVarBoolean("Reg. to previous frame", false);
    EzVarEnum<ResizePolicy> resizePolicy = new EzVarEnum<ResizePolicy>("Resize policy", ResizePolicy.values());
    
    // advanced mode //
    
    EzVarSequence refSeq = new EzVarSequence("Reference");
    EzVarChannel refSeqC = new EzVarChannel("Ref. channel", refSeq.getVariable(), false);
    EzVarSequence objSeq = new EzVarSequence("Candidate");
    EzVarChannel objSeqC = new EzVarChannel("Candidate channel", objSeq.getVariable(), false);
    EzVarSequence applyTo = new EzVarSequence("Apply transform to");
    EzVarChannel applyToC = new EzVarChannel("Channel(s)", applyTo.getVariable(), true);
    EzVarBoolean preserveSize = new EzVarBoolean("Preserve size", true);
    
    @Override
    protected void initialize()
    {
        setTimeDisplay(true);
        
        // type of registration
        
        addEzComponent(type);
        
        addEzComponent(mode);
        
        // simple mode
        
        EzLabel simpleLabel = new EzLabel("Registers a dataset based on a specific channel / slice / frame");
        final EzGroup simple = new EzGroup("Simple mode", simpleLabel, seq, refC, refT, refZ, regToPrevT, resizePolicy,
                applyToSimple);
        type.addVisibilityTriggerTo(resizePolicy, RegType.Temporal2D);
        type.addVisibilityTriggerTo(regToPrevT, RegType.Temporal2D);
        regToPrevT.addVisibilityTriggerTo(refT, false);
        addEzComponent(simple);
        mode.addVisibilityTriggerTo(simple, RegMode.Simple);
        
        // advanced mode (for chromatic registration only)
        
        EzLabel advancedLabel = new EzLabel(
                "Calculates the transform between two datasets, and applies it to a third dataset");
        EzGroup advanced = new EzGroup("Advanced mode", advancedLabel, refSeq, refSeqC, objSeq, objSeqC, applyTo,
                applyToC, preserveSize);
        addEzComponent(advanced);
        mode.addVisibilityTriggerTo(advanced, RegMode.Advanced);
        
        type.addVarChangeListener(new EzVarListener<RigidRegistration.RegType>()
        {
            @Override
            public void variableChanged(EzVar<RegType> source, RegType newValue)
            {
                // offer advanced mode for chromatic registration only
                if (newValue == RegType.Chromatic2D)
                {
                    mode.setVisible(true);
                    if (mode.getValue() == RegMode.Simple)
                    	applyToSimple.setVisible(false);
                }
                else
                {
                    mode.setVisible(false);
                    mode.setValue(RegMode.Simple);
                    simple.setVisible(true);
                    applyToSimple.setVisible(true);
                }
            }
        });
        
    }
	// declare ourself to Blocks
	@Override
    public void declareInput(VarList inputMap)
    {
		inputMap.add(seq.name, seq.getVariable());
		inputMap.add(refC.name, refC.getVariable());
		inputMap.add(refT.name, refT.getVariable());
		inputMap.add(refZ.name, refZ.getVariable());
		inputMap.add(regToPrevT.name, regToPrevT.getVariable());
		inputMap.add(resizePolicy.name, resizePolicy.getVariable());
		inputMap.add(applyToSimple.name, applyToSimple.getVariable());
	}

	// declare ourself to Blocks
	@Override
    public void declareOutput(VarList outputMap)
    {
		outputMap.add(seqVar.getName(), seqVar);
		outputMap.add(applyToSimpleVar.getName(), applyToSimpleVar);
	}

    @Override
    protected void execute()
    {
        boolean rotate = false;
        
        
        if (isHeadLess())
        {
        	mode.setValue(RegMode.Simple);
        	type.setValue(RegType.Temporal2D);
        }

        
        try
        {
        if (mode.getValue() == RegMode.Simple)
          {
           Sequence s = seq.getValue(true);
            
            switch (type.getValue())
            {
                case Chromatic2D:
                    correctChromaticTranslation2D(s, refC.getValue(), refT.getValue(), refZ.getValue());
                    rotate = correctChromaticRotation2D(s, refC.getValue(), refT.getValue(), refZ.getValue());
                        if (rotate)
                            correctChromaticTranslation2D(s, refC.getValue(), refT.getValue(), refZ.getValue());
                break;
            
                case Temporal2D:
                	Sequence applyToSeq = applyToSimple.getValue(false);
                    if (regToPrevT.getValue())
                    {
                        correctTemporalTranslation2D(s, refC.getValue(), refZ.getValue(),applyToSeq);
                        rotate = correctTemporalRotation2D(s, refC.getValue(), refZ.getValue(), applyToSeq);
                            if (rotate)
                                correctTemporalTranslation2D(s, refC.getValue(), refZ.getValue(), applyToSeq);
                    }
                    else
                    {
                            correctTemporalTranslation2D(s, refT.getValue(), refC.getValue(), refZ.getValue(),
                                    applyToSeq);
                            rotate = correctTemporalRotation2D(s, refT.getValue(), refC.getValue(), refZ.getValue(),
                                    applyToSeq);
                            if (rotate)
                                correctTemporalTranslation2D(s, refT.getValue(), refC.getValue(), refZ.getValue(),
                                        applyToSeq);
                    }
                    seqVar.setValue(s);
                    applyToSimpleVar.setValue(applyToSeq);
                break;
          }
	}
        else
          // Advanced => chromatic only (for now)
        {
            Sequence referenceSeq = refSeq.getValue(true);
            int referenceChannel = refSeqC.getValue();
            
            Sequence candidateSeq = objSeq.getValue(true);
            int candidateChannel = objSeqC.getValue();
            
            Sequence applyToSeq = applyTo.getValue(true);
            int applyToChannel = applyToC.getValue();
            
            Vector2d translation;
            double angle;
            	Sequence tmpCandidate = SequenceUtil.getCopy(candidateSeq);
                // 1) Translation
                translation = findChromaticTranslation2D(tmpCandidate, candidateChannel, referenceSeq,
                        referenceChannel);
                if (translation.lengthSquared() != 0)
                {
                    applyTranslation2D(tmpCandidate, -1, -1, candidateChannel, translation, preserveSize.getValue());
                    applyTranslation2D(applyToSeq, -1, -1, applyToChannel, translation, preserveSize.getValue());
                }
                
                // 2) Rotation
                
                // Check if the data size is different due to the previous translation
                if (preserveSize.getValue() || translation.lengthSquared() == 0.0)
                    translation = null;
                
                angle = findChromaticRotation2D(tmpCandidate, candidateChannel, referenceSeq, referenceChannel,
                        translation);
                
                if (Math.abs(angle % Math.PI) > 1e-10)
                {
                    applyRotation2D(tmpCandidate, -1, -1, candidateChannel, angle, preserveSize.getValue());
                    applyRotation2D(applyToSeq, -1, -1, applyToChannel, angle, preserveSize.getValue());
                    
                    // 3) Extra translation if necessary
                    translation = findChromaticTranslation2D(tmpCandidate, candidateChannel, referenceSeq,
                            referenceChannel);
                    
                    if (translation.lengthSquared() != 0)
                    {
                        applyTranslation2D(applyToSeq, -1, -1, applyToChannel, translation, preserveSize.getValue());
                    }
                }
            }
            }
            
        catch (InterruptedException e)
        {
            new FailedAnnounceFrame("Process interrupted...");
        }
    }
    
    @Override
    public void clean()
    {
        
        //
    }
    
    // HIGH-LEVEL METHODS //
    
    /**
     * Register rotation in a sequence over time using a single channel as reference
     * 
     * @param sequence
     *            the sequence to register
     * @param referenceChannel
     *            the reference channel used to register other channels
     * @param referenceFrame
     *            the frame used to calculate the transform (or -1 to calculate an average transform
     *            using all frames)
     * @param referenceSlice
     *            the slice used to calculate the transform (or -1 to calculate an average transform
     *            using all slices)
     * @return <code>true</code> if the data has changed, <code>false</code> otherwise (i.e. the
     *         registration cannot be improved)
     */
    public static boolean correctChromaticRotation2D(Sequence sequence, int referenceChannel, int referenceFrame,
            int referenceSlice)
    {
        boolean change = false;
        
        for (int c = 0; c < sequence.getSizeC(); c++)
        {
            if (c == referenceChannel)
                continue;
            
            double angle = 0.0;
            int n = 0;
            
            int minT = referenceFrame == -1 ? 0 : referenceFrame;
            int maxT = referenceFrame == -1 ? sequence.getSizeT() : referenceFrame;
            
            for (int t = minT; t <= maxT; t++)
            {
                if (Thread.currentThread().isInterrupted())
                    return change;
                
                int minZ = referenceSlice == -1 ? 0 : referenceSlice;
                int maxZ = referenceSlice == -1 ? sequence.getSizeZ(t) : referenceSlice;
                
                for (int z = minZ; z <= maxZ; z++)
                {
                    IcyBufferedImage img = sequence.getImage(t, z);
                    
                    angle += findRotation2D(img, c, img, referenceChannel);
                    n++;
                }
            }
            
            angle /= n;
            System.out.println("[Rigid Registration] Angle: " + angle);
            
            if (angle != 0.0)
            {
                change = true;
                for (int t = 0; t < sequence.getSizeT(); t++)
                    for (int z = 0; z < sequence.getSizeZ(t); z++)
                        sequence.setImage(t, z, applyRotation2D(sequence.getImage(t, z), c, angle, true));
            }
        }
        
        return change;
    }
    
    /**
     * Register a sequence over time using the previous frame as reference
     * 
     * @param sequence
     *            the sequence to register
     * @param referenceChannel
     *            the channel used to calculate the transform (or -1 to calculate an average
     *            transform using all channels)
     * @param referenceSlice
     *            the slice used to calculate the transform (or -1 to calculate an average transform
     *            using all slices)
     * @return <code>true</code> if the data has changed, <code>false</code> otherwise (i.e. the
     *         registration cannot be improved)
     */
    public static boolean correctTemporalRotation2D(Sequence sequence, int referenceChannel, int referenceSlice)
    {
    	return correctTemporalRotation2D(sequence, referenceChannel, referenceSlice, (Sequence) null);
    }
    public static boolean correctTemporalRotation2D(Sequence sequence, int referenceChannel, int referenceSlice,
            Sequence applyToSeq)
    {
        boolean change = false;
        
        for (int t = 1; t < sequence.getSizeT(); t++)
        {
            int referenceFrame = t - 1;
            
            if (Thread.currentThread().isInterrupted())
                return change;
            
            double angle = 0.0;
            int n = 0;
            
            int minZ = referenceSlice == -1 ? 0 : referenceSlice;
            int maxZ = referenceSlice == -1 ? sequence.getSizeZ(t) : referenceSlice;
            
            for (int z = minZ; z <= maxZ; z++)
            {
                int minC = referenceChannel == -1 ? 0 : referenceChannel;
                int maxC = referenceChannel == -1 ? sequence.getSizeC() : referenceChannel;
                
                for (int c = minC; c <= maxC; c++)
                {
                    IcyBufferedImage img = sequence.getImage(t, z);
                    IcyBufferedImage ref = sequence.getImage(referenceFrame, z);
                    
                    angle += findRotation2D(img, c, ref, c);
                    n++;
                }
            }
            
            angle /= n;
            System.out.println("[Rigid Registration] Angle: " + angle);
            
            if (angle != 0.0)
            {
                change = true;
                
                for (int z = 0; z < sequence.getSizeZ(t); z++)
                    sequence.setImage(t, z, applyRotation2D(sequence.getImage(t, z), -1, angle, true));
                
                if (applyToSeq != null)
                {
                	for (int z = 0; z < applyToSeq.getSizeZ(t); z++)
                		applyToSeq.setImage(t, z, applyRotation2D(applyToSeq.getImage(t, z), -1, angle, true));
                }
           }
        }
        
        return change;
    }
    
    /**
     * Register a sequence over time using a single frame as reference
     * 
     * @param sequence
     *            the sequence to register
     * @param referenceFrame
     *            the reference frame used to register other frames
     * @param referenceChannel
     *            the channel used to calculate the transform (or -1 to calculate an average
     *            transform using all channels)
     * @param referenceSlice
     *            the slice used to calculate the transform (or -1 to calculate an average transform
     *            using all slices)
     * @return <code>true</code> if the data has changed, <code>false</code> otherwise (i.e. the
     *         registration cannot be improved)
     */
    public static boolean correctTemporalRotation2D(Sequence sequence, int referenceFrame, int referenceChannel,
            int referenceSlice)
    {
    	return correctTemporalRotation2D(sequence, referenceFrame, referenceChannel, referenceSlice, (Sequence) null);
    }
    public static boolean correctTemporalRotation2D(Sequence sequence, int referenceFrame, int referenceChannel,
            int referenceSlice, Sequence applyToSeq)
    {
        boolean change = false;
        
        for (int t = 0; t < sequence.getSizeT(); t++)
        {
            if (Thread.currentThread().isInterrupted())
                return change;
            
            if (t == referenceFrame)
                continue;
            
            double angle = 0.0;
            int n = 0;
            
            int minZ = referenceSlice == -1 ? 0 : referenceSlice;
            int maxZ = referenceSlice == -1 ? sequence.getSizeZ(t) : referenceSlice;
            
            for (int z = minZ; z <= maxZ; z++)
            {
                int minC = referenceChannel == -1 ? 0 : referenceChannel;
                int maxC = referenceChannel == -1 ? sequence.getSizeC() : referenceChannel;
                
                for (int c = minC; c <= maxC; c++)
                {
                    IcyBufferedImage img = sequence.getImage(t, z);
                    IcyBufferedImage ref = sequence.getImage(referenceFrame, z);
                    
                    angle += findRotation2D(img, c, ref, c);
                    n++;
                }
            }
            
            angle /= n;
            System.out.println("[Rigid Registration] Angle: " + angle);
            
            if (angle != 0.0)
            {
                change = true;
                
                for (int z = 0; z < sequence.getSizeZ(t); z++)
                    sequence.setImage(t, z, applyRotation2D(sequence.getImage(t, z), -1, angle, true));
                if (applyToSeq != null)
                {
                	for (int z = 0; z < applyToSeq.getSizeZ(t); z++)
                		applyToSeq.setImage(t, z, applyRotation2D(applyToSeq.getImage(t, z), -1, angle, true));
                }
            }
        }
        
        return change;
    }
    
    /**
     * Register translation in a sequence over time using a single channel as reference
     * 
     * @param sequence
     *            the sequence to register
     * @param referenceChannel
     *            the reference channel used to register other channels
     * @param referenceFrame
     *            the frame used to calculate the transform (or -1 to calculate an average transform
     *            using all frames)
     * @param referenceSlice
     *            the slice used to calculate the transform (or -1 to calculate an average transform
     *            using all slices)
     * @return <code>true</code> if the data has changed, <code>false</code> otherwise (i.e. the
     *         registration cannot be improved)
     */
    public static boolean correctChromaticTranslation2D(Sequence sequence, int referenceChannel, int referenceFrame,
            int referenceSlice)
    {
        boolean change = false;
        
        for (int c = 0; c < sequence.getSizeC(); c++)
        {
            if (c == referenceChannel)
                continue;
            
            Vector2d translation = new Vector2d();
            int n = 0;
            
            int minT = referenceFrame == -1 ? 0 : referenceFrame;
            int maxT = referenceFrame == -1 ? sequence.getSizeT() : referenceFrame;
            
            for (int t = minT; t <= maxT; t++)
            {
                if (Thread.currentThread().isInterrupted())
                    return change;
                
                int minZ = referenceSlice == -1 ? 0 : referenceSlice;
                int maxZ = referenceSlice == -1 ? sequence.getSizeZ(t) : referenceSlice;
                
                for (int z = minZ; z <= maxZ; z++)
                {
                    IcyBufferedImage img = sequence.getImage(t, z);
                    
                    translation.add(findTranslation2D(img, c, img, referenceChannel));
                    n++;
                }
            }
            
            translation.scale(1.0 / n);
            System.out.println("[Rigid Registration] Translation: " + StringUtil.toString(translation.x, 2) + " / "
                    + StringUtil.toString(translation.y, 2));
            
            if (translation.lengthSquared() != 0)
            {
                change = true;
                
                for (int t = 0; t < sequence.getSizeT(); t++)
                    for (int z = 0; z < sequence.getSizeZ(t); z++)
                        sequence.setImage(t, z, applyTranslation2D(sequence.getImage(t, z), c, translation, true));
            }
        }
        
        return change;
    }
    
    /**
     * Register a sequence over time using the previous frame as reference
     * 
     * @param sequence
     *            the sequence to register
     * @param referenceChannel
     *            the channel used to calculate the transform (or -1 to calculate an average
     *            transform using all channels)
     * @param referenceSlice
     *            the slice used to calculate the transform (or -1 to calculate an average transform
     *            using all slices)
     * @return <code>true</code> if the data has changed, <code>false</code> otherwise (i.e. the
     *         registration cannot be improved)
     */
    public static boolean correctTemporalTranslation2D(Sequence sequence, int referenceChannel, int referenceSlice)
    {
    	return correctTemporalTranslation2D(sequence, referenceChannel, referenceSlice, null);
    }
    public static boolean correctTemporalTranslation2D(Sequence sequence, int referenceChannel, int referenceSlice,
            Sequence applyToSeq)
    {
        boolean change = false;
        
        for (int t = 1; t < sequence.getSizeT(); t++)
        {
            int referenceFrame = t - 1;
            
            if (Thread.currentThread().isInterrupted())
                return change;
            
            Vector2d translation = new Vector2d();
            int n = 0;
            
            int minZ = referenceSlice == -1 ? 0 : referenceSlice;
            int maxZ = referenceSlice == -1 ? sequence.getSizeZ(t) : referenceSlice;
            
            for (int z = minZ; z <= maxZ; z++)
            {
                int minC = referenceChannel == -1 ? 0 : referenceChannel;
                int maxC = referenceChannel == -1 ? sequence.getSizeC() - 1 : referenceChannel;
                
                for (int c = minC; c <= maxC; c++)
                {
                    IcyBufferedImage img = sequence.getImage(t, z);
                    IcyBufferedImage ref = sequence.getImage(referenceFrame, z);
                    
                    translation.add(findTranslation2D(img, c, ref, c));
                    n++;
                }
            }
            
            translation.scale(1.0 / n);
            System.out.println("[Rigid Registration] Translation: " + StringUtil.toString(translation.x, 2) + " / "
                    + StringUtil.toString(translation.y, 2));
            
            if (translation.lengthSquared() != 0)
            {
                change = true;
                
                for (int z = 0; z < sequence.getSizeZ(t); z++)
                    sequence.setImage(t, z, applyTranslation2D(sequence.getImage(t, z), -1, translation, true));
                
                if (applyToSeq != null) 
                {
                	for (int z = 0; z < applyToSeq.getSizeZ(t); z++)
                		applyToSeq.setImage(t, z, applyTranslation2D(applyToSeq.getImage(t, z), -1, translation, true));
                }                
            }
        }
        
        return change;
    }
    
    /**
     * Register a sequence over time using a single frame as reference
     * 
     * @param sequence
     *            the sequence to register
     * @param referenceFrame
     *            the reference frame used to register other frames
     * @param referenceChannel
     *            the channel used to calculate the transform (or -1 to calculate an average
     *            transform using all channels)
     * @param referenceSlice
     *            the slice used to calculate the transform (or -1 to calculate an average transform
     *            using all slices)
     * @return <code>true</code> if the data has changed, <code>false</code> otherwise (i.e. the
     *         registration cannot be improved)
     * @throws InterruptedException
     */
    public static boolean correctTemporalTranslation2D(Sequence sequence, int referenceFrame, int referenceChannel,
            int referenceSlice) throws InterruptedException
    {
        return correctTemporalTranslation2D(sequence, referenceFrame, referenceChannel, referenceSlice,
                (Sequence) null);
    }
    public static boolean correctTemporalTranslation2D(Sequence sequence, int referenceFrame, int referenceChannel,
            int referenceSlice, Sequence applyToSeq) throws InterruptedException
    {
        Rectangle newBounds = correctTemporalTranslation2D(sequence, referenceFrame, referenceChannel, referenceSlice,
                ResizePolicy.PRESERVE_SIZE, null, applyToSeq);
        
        return newBounds.equals(sequence.getBounds2D());
    }
    
    /**
     * Registers a 2D translation/drift in a sequence using a single frame as reference
     * 
     * @param sequence
     *            the sequence to register
     * @param referenceFrame
     *            the reference frame used to register other frames
     * @param referenceChannel
     *            the channel used to calculate the transform (or -1 to calculate an average
     *            transform using all channels)
     * @param referenceSlice
     *            the slice used to calculate the transform (or -1 to calculate an average transform
     *            using all slices)
     * @param policy
     *            indicates how to deal with out-of-bound and missing pixels (see
     *            {@link ResizePolicy}) for more details
     * @param status
     *            (<code>null</code> if not needed) a status object to let the user know where it's
     *            at (just like All Saints).
     * @return the smallest common bounds enclosing the registered data (or the original sequence
     *         bounds if no drift was detected)
     * @throws InterruptedException
     */
    public static Rectangle correctTemporalTranslation2D(Sequence sequence, int referenceFrame, int referenceChannel,
            int referenceSlice, ResizePolicy policy, EzStatus status) throws InterruptedException
    {
        return correctTemporalTranslation2D(sequence, referenceFrame, referenceChannel, referenceSlice, policy, status,
                (Sequence) null);
    }
    public static Rectangle correctTemporalTranslation2D(Sequence sequence, int referenceFrame, int referenceChannel,
            int referenceSlice, ResizePolicy policy, EzStatus status, Sequence applyToSeq) throws InterruptedException
    {
   	
        if (status == null)
            status = new EzStatus();
        
        Rectangle initialBounds = sequence.getBounds2D();
        int sizeT = sequence.getSizeT();
        int sizeC = sequence.getSizeC();
        DataType dataType = sequence.getDataType_();
        
        Sequence output = policy == ResizePolicy.PRESERVE_SIZE ? sequence : new Sequence();
        
        //
        Rectangle initialBoundsApplySeq = null;
        int sizeTApplySeq = 0;
        int sizeCApplySeq = 0;
        DataType dataTypeApplySeq = null;
        Sequence outputApplySeq = null;
        
        if (applyToSeq != null)
        {
            initialBoundsApplySeq = applyToSeq.getBounds2D();
            sizeTApplySeq = applyToSeq.getSizeT();
            sizeCApplySeq = applyToSeq.getSizeC();
            dataTypeApplySeq = applyToSeq.getDataType_();
            
            outputApplySeq = policy == ResizePolicy.PRESERVE_SIZE ? applyToSeq : new Sequence();        	
        }
        
        // Step 1: compute and accumulate all translations
        
        ArrayList<Rectangle> translatedBounds = new ArrayList<Rectangle>(sizeT);
        ArrayList<Rectangle> translatedBoundsApplySeq = null;
        if (applyToSeq != null)
        	translatedBoundsApplySeq = new ArrayList<Rectangle>(sizeTApplySeq);
        
        for (int t = 0; t < sizeT; status.setCompletion(++t / (double) sizeT))
        {
            if (Thread.currentThread().isInterrupted())
                return initialBounds;
            
            if (t == referenceFrame)
            {
                translatedBounds.add(initialBounds);
                if (applyToSeq != null)
                	translatedBoundsApplySeq.add(initialBoundsApplySeq);
            }
            else
            {
                Point translation = findTranslation2D(sequence, referenceChannel, referenceSlice, t, referenceFrame);
                
                if (translation.x != 0 || translation.y != 0)
                {
                    translatedBounds.add(
                            new Rectangle(translation.x, translation.y, initialBounds.width, initialBounds.height));
                    if (applyToSeq != null)
                        translatedBoundsApplySeq.add(new Rectangle(translation.x, translation.y,
                                initialBoundsApplySeq.width, initialBoundsApplySeq.height));
                }
                else
                {
                    translatedBounds.add(initialBounds);
                    if (applyToSeq != null)
                    	translatedBoundsApplySeq.add(initialBoundsApplySeq);
                }
            }
        }
        
        // Step 2: compute the final bounds
        
        Rectangle finalBounds = new Rectangle(initialBounds);
        
        if (policy == ResizePolicy.INTERSECT_BOUNDS)
            for (Rectangle bounds : translatedBounds)
        {
            finalBounds = finalBounds.intersection(bounds);
        }
        else if (policy == ResizePolicy.UNITE_BOUNDS)
            for (Rectangle bounds : translatedBounds)
        {
            finalBounds = finalBounds.union(bounds);
        }
        
        //
        Rectangle finalBoundsApplySeq = null;
        if (applyToSeq != null)
        {
        	finalBoundsApplySeq = new Rectangle(initialBoundsApplySeq);
        
            if (policy == ResizePolicy.INTERSECT_BOUNDS)
                for (Rectangle bounds : translatedBoundsApplySeq)
        	{
        		finalBoundsApplySeq = finalBoundsApplySeq.intersection(bounds);
        	}
            else if (policy == ResizePolicy.UNITE_BOUNDS)
                for (Rectangle bounds : translatedBoundsApplySeq)
        	{
        		finalBoundsApplySeq = finalBoundsApplySeq.union(bounds);
        	}
        }
        // Step 3: build the final sequence
        
        for (int t = 0; t < sizeT; status.setCompletion(++t / (double) sizeT))
        {
            if (Thread.currentThread().isInterrupted())
                return initialBounds;
            
            Point newOrigin = translatedBounds.get(t).getLocation();
            newOrigin.translate(-finalBounds.x, -finalBounds.y);
            
            Point newOriginApplySeq = null;
            if (applyToSeq != null)
            {
            	 newOriginApplySeq = translatedBoundsApplySeq.get(t).getLocation();
            	 newOriginApplySeq.translate(-finalBoundsApplySeq.x, -finalBoundsApplySeq.y);
            }            
            
            for (int z = 0; z < sequence.getSizeZ(t); z++)
            {
                IcyBufferedImage translatedImage = new IcyBufferedImage(finalBounds.width, finalBounds.height, sizeC,
                        dataType);
                translatedImage.copyData(sequence.getImage(t, z), null, newOrigin);
                output.setImage(t, z, translatedImage);
            }
            
            if (applyToSeq != null)
            {
            	for (int z = 0; z < applyToSeq.getSizeZ(t); z++)
            	{
                    IcyBufferedImage translatedImage = new IcyBufferedImage(finalBoundsApplySeq.width,
                            finalBoundsApplySeq.height, sizeCApplySeq, dataTypeApplySeq);
            		translatedImage.copyData(applyToSeq.getImage(t, z), null, newOriginApplySeq);
            		outputApplySeq.setImage(t, z, translatedImage);
            	}
            }

        }
        
        if (policy != ResizePolicy.PRESERVE_SIZE)
        {
    			sequence.copyDataFrom(output);
    			if (applyToSeq != null)
        				applyToSeq.copyDataFrom(outputApplySeq);
        			
        }
        
        return finalBounds;
    }
    
    /**
     * Registers a 2D translation/drift in a sequence using the previous frame as reference
     * 
     * @param sequence
     *            the sequence to register
     * @param referenceChannel
     *            the channel used to calculate the transform (or -1 to calculate an average
     *            transform using all channels)
     * @param referenceSlice
     *            the slice used to calculate the transform (or -1 to calculate an average transform
     *            using all slices)
     * @param policy
     *            indicates how to deal with out-of-bound and missing pixels (see
     *            {@link ResizePolicy}) for more details
     * @param status
     *            (<code>null</code> if not needed) a status object to let the user know where it's
     *            at (just like All Saints).
     * @return the smallest common bounds enclosing the registered data (or the original sequence
     *         bounds if no drift was detected)
     * @throws InterruptedException
     */
    public static Rectangle correctTemporalTranslation2D(Sequence sequence, int referenceChannel, int referenceSlice,
            ResizePolicy policy, EzStatus status) throws InterruptedException
    {
        if (status == null)
            status = new EzStatus();
        
        Rectangle initialBounds = sequence.getBounds2D();
        int sizeT = sequence.getSizeT();
        int sizeC = sequence.getSizeC();
        DataType dataType = sequence.getDataType_();
        
        Sequence output = policy == ResizePolicy.PRESERVE_SIZE ? sequence : new Sequence();
        
        // Step 1: compute and accumulate all translations
        
        ArrayList<Rectangle> translatedBounds = new ArrayList<Rectangle>(sizeT);
        
        // Don't process t=0, we know the answer...
        translatedBounds.add(initialBounds);
        
        // Start directly at t=1
        for (int t = 1; t < sizeT; status.setCompletion(++t / (double) sizeT))
        {
            if (Thread.currentThread().isInterrupted())
                return initialBounds;
            
            Point translation = findTranslation2D(sequence, referenceChannel, referenceSlice, t, t - 1);
            
            if (translation.x != 0 || translation.y != 0)
            {
                translatedBounds
                        .add(new Rectangle(translation.x, translation.y, initialBounds.width, initialBounds.height));
            }
            else
            {
                translatedBounds.add(initialBounds);
            }
        }
        
        // Step 2: compute the final bounds
        
        Rectangle cumulativeBounds = new Rectangle(initialBounds);
        Rectangle finalBounds = new Rectangle(initialBounds);
        
        if (policy == ResizePolicy.INTERSECT_BOUNDS)
            for (Rectangle bounds : translatedBounds)
        {
            cumulativeBounds.translate(bounds.x, bounds.y);
            finalBounds = finalBounds.intersection(cumulativeBounds);
        }
        else if (policy == ResizePolicy.UNITE_BOUNDS)
            for (Rectangle bounds : translatedBounds)
        {
            cumulativeBounds.translate(bounds.x, bounds.y);
            finalBounds = finalBounds.union(cumulativeBounds);
        }
        
        // Step 3: build the final sequence
        
        cumulativeBounds.setBounds(initialBounds);
        
        for (int t = 0; t < sizeT; status.setCompletion(++t / (double) sizeT))
        {
            if (Thread.currentThread().isInterrupted())
                return initialBounds;
            
            Rectangle translated = translatedBounds.get(t);
            cumulativeBounds.translate(translated.x, translated.y);
            
            for (int z = 0; z < sequence.getSizeZ(t); z++)
            {
                IcyBufferedImage translatedImage = new IcyBufferedImage(finalBounds.width, finalBounds.height, sizeC,
                        dataType);
                translatedImage.copyData(sequence.getImage(t, z), null,
                        new Point(cumulativeBounds.x - finalBounds.x, cumulativeBounds.y - finalBounds.y));
                output.setImage(t, z, translatedImage);
            }
        }

        if (policy != ResizePolicy.PRESERVE_SIZE)
            sequence.copyDataFrom(output);
        
        return finalBounds;
    }
    
    /**
     * Finds the 2D translation between two frames of a given sequence and return the result as a
     * {@link Point} with integer coordinates (rounded). If the reference channel or slice is set to
     * <code>-1</code> (i.e. "ALL"), then an average translation is computed from all selected
     * channels and slices
     * 
     * @param sequence
     * @param referenceChannel
     * @param referenceSlice
     * @param candidateFrame
     * @param referenceFrame
     * @return
     */
    private static Point findTranslation2D(Sequence sequence, int referenceChannel, int referenceSlice,
            int candidateFrame, int referenceFrame)
    {
        Vector2d translation = new Vector2d();
        
        int n = 0;
        int minZ = referenceSlice == -1 ? 0 : referenceSlice;
        int maxZ = referenceSlice == -1 ? sequence.getSizeZ(candidateFrame) : referenceSlice;
        for (int z = minZ; z <= maxZ; z++)
        {
            int minC = referenceChannel == -1 ? 0 : referenceChannel;
            int maxC = referenceChannel == -1 ? sequence.getSizeC() - 1 : referenceChannel;
            
            for (int c = minC; c <= maxC; c++)
            {
                IcyBufferedImage img = sequence.getImage(candidateFrame, z);
                IcyBufferedImage ref = sequence.getImage(referenceFrame, z);
                
                translation.add(findTranslation2D(img, c, ref, c));
                n++;
            }
        }
        if (n > 1)
            translation.scale(1.0 / n);
        
        return new Point((int) Math.round(translation.x), (int) Math.round(translation.y));
    }
    
    /**
     * Calculates the chromatic rotation angle in 2D necessary to transform a source sequence into a
     * target sequence. If the sequences have more than one frame or slice, the calculation is
     * performed and averaged over all planes (in Z and T).
     * 
     * @param source
     * @param sourceC
     * @param target
     * @param targetC
     * @return
     */
    public static double findChromaticRotation2D(Sequence source, int sourceC, Sequence target, int targetC)
    {
        return findChromaticRotation2D(source, sourceC, target, targetC, null);
    }
    
    /**
     * Calculates the chromatic rotation angle in 2D necessary to transform a source sequence into a
     * target sequence. If the sequences have more than one frame or slice, the calculation is
     * performed and averaged over all planes (in Z and T).
     * 
     * @param source
     * @param sourceC
     * @param target
     * @param targetC
     * @param previousTranslation
     *            the previous translation (if any) necessary to transform source into target
     * @return
     */
    public static double findChromaticRotation2D(Sequence source, int sourceC, Sequence target, int targetC,
            Vector2d previousTranslation)
    {
        int sizeT = source.getSizeT();
        int sizeZ = source.getSizeZ();
        
        if (sizeT != target.getSizeT() || sizeZ != target.getSizeZ())
        {
            throw new IllegalArgumentException("Source and target sequences have different (Z,T) dimensions");
        }
        
        double angle = 0;
        for (int t = 0; t < sizeT; t++)
            for (int z = 0; z < sizeZ; z++)
            {
                IcyBufferedImage srcImg = source.getImage(t, z);
                IcyBufferedImage tgtImg = target.getImage(t, z);
                angle += findRotation2D(srcImg, sourceC, tgtImg, targetC, previousTranslation);
            }
        angle /= (sizeT * sizeZ);
        return angle;
    }
    
    /**
     * Calculates the chromatic translation vector in 2D necessary to transform a source sequence
     * into a target sequence. If the sequences have more than one frame or slice, the calculation
     * is performed and averaged over all planes (in Z and T).
     * 
     * @param source
     * @param sourceC
     * @param target
     * @param targetC
     * @return
     */
    public static Vector2d findChromaticTranslation2D(Sequence source, int sourceC, Sequence target, int targetC)
    {
        int sizeT = source.getSizeT();
        int sizeZ = source.getSizeZ();
        
        if (sizeT != target.getSizeT() || sizeZ != target.getSizeZ())
        {
            throw new IllegalArgumentException("Source and target sequences have different (Z,T) dimensions");
        }
        
        Vector2d vector = new Vector2d();
        for (int t = 0; t < sizeT; t++)
            for (int z = 0; z < sizeZ; z++)
            {
                IcyBufferedImage srcImg = source.getImage(t, z);
                IcyBufferedImage tgtImg = target.getImage(t, z);
                vector.add(findTranslation2D(srcImg, sourceC, tgtImg, targetC));
            }
        vector.scale(1.0 / (sizeT * sizeZ));
        return vector;
    }
    
    /**
     * @param source
     *            the source image
     * @param sourceC
     *            the channel in the source image
     * @param target
     *            the target image
     * @param targetC
     *            the channel in the target image
     * @return the rotation angle around the center (in radians) that transforms source into target
     */
    public static double findRotation2D(IcyBufferedImage source, int sourceC, IcyBufferedImage target, int targetC)
    {
        return findRotation2D(source, sourceC, target, targetC, null);
    }
    
    /**
     * @param source
     *            the source image
     * @param sourceC
     *            the channel in the source image
     * @param target
     *            the target image
     * @param targetC
     *            the channel in the target image
     * @param previousTranslation
     *            the previous translation (if any) necessary to register the source to the target
     * @return the rotation angle around the center (in radians) that transforms source into target
     */
    public static double findRotation2D(IcyBufferedImage source, int sourceC, IcyBufferedImage target, int targetC,
            Vector2d previousTranslation)
    {
        if (!source.getBounds().equals(target.getBounds()))
        {
            // Both sizes are different. What to do?
            
            if (previousTranslation != null)
            {
                // the source has most probably been translated previously, let's grow the target
                // accordingly
                // (just need to know where the original data has to go)
                int xAlign = previousTranslation.x > 0 ? SwingConstants.LEFT : SwingConstants.RIGHT;
                int yAlign = previousTranslation.y > 0 ? SwingConstants.TOP : SwingConstants.BOTTOM;
                target = IcyBufferedImageUtil.scale(target, source.getSizeX(), source.getSizeY(), false, xAlign,
                        yAlign);
            }
            
            else
                throw new UnsupportedOperationException("Cannot register images of different size (yet)");
        }
        
        // Convert to Log-Polar
        
        IcyBufferedImage sourceLogPol = toLogPolar(source.getImage(sourceC));
        IcyBufferedImage targetLogPol = toLogPolar(target.getImage(targetC));
        
        int width = sourceLogPol.getWidth(), height = sourceLogPol.getHeight();
        
        float[] _sourceLogPol = sourceLogPol.getDataXYAsFloat(0);
        float[] _targetLogPol = targetLogPol.getDataXYAsFloat(0);
        
        // Compute spectral correlation
        
        float[] correlationMap = spectralCorrelation(_sourceLogPol, _targetLogPol, width, height);
        
        // Find maximum correlation (=> rotation)
        
        int argMax = argMax(correlationMap, correlationMap.length / 2);
        
        // rotation is given along the X axis
        int rotX = argMax % width;
        
        if (rotX > width / 2)
            rotX -= width;
        
        return -rotX * 2 * Math.PI / width;
    }
    
    /**
     * @param source
     *            the source image
     * @param sourceC
     *            the channel in the source image
     * @param target
     *            the target image
     * @param targetC
     *            the channel in the target image
     * @return the translation vector needed to transform source into target
     */
    public static Vector2d findTranslation2D(IcyBufferedImage source, int sourceC, IcyBufferedImage target, int targetC)
    {
        if (!source.getBounds().equals(target.getBounds()))
            throw new UnsupportedOperationException("Cannot register images of different size (yet)");
        
        int width = source.getWidth();
        int height = source.getHeight();
        
        float[] _source = Array1DUtil.arrayToFloatArray(source.getDataXY(sourceC), source.isSignedDataType());
        float[] _target = Array1DUtil.arrayToFloatArray(target.getDataXY(targetC), target.isSignedDataType());
        
        float[] correlationMap = spectralCorrelation(_source, _target, width, height);
        
        // IcyBufferedImage corr = new IcyBufferedImage(width, height, new
        // float[][]{correlationMap});
        // Icy.getMainInterface().addSequence(new Sequence(corr));
        
        // Find maximum correlation
        
        int argMax = argMax(correlationMap, correlationMap.length);
        
        int transX = argMax % width;
        int transY = argMax / width;
        
        if (transX > width / 2)
            transX -= width;
        if (transY > height / 2)
            transY -= height;
        
        // recover (x,y)
        return new Vector2d(-transX, -transY);
    }
    
    /**
     * Rotates the specified sequence
     * 
     * @param seq
     *            the sequence to translate
     * @param t
     *            the frame to translate (or -1 for all)
     * @param z
     *            the frame to translate (or -1 for all)
     * @param c
     *            the frame to translate (or -1 for all)
     * @param preserveImageSize
     */
    public static void applyRotation2D(Sequence seq, int t, int z, int c, double angle, boolean preserveImageSize)
    {
        if (angle == 0.0)
            return;
        
        int minT = (t == -1 ? 0 : t), maxT = (t == -1 ? seq.getSizeT() - 1 : t);
        int minZ = (z == -1 ? 0 : z), maxZ = (z == -1 ? seq.getSizeZ() - 1 : z);
        
        for (int time = minT; time <= maxT; time++)
            for (int slice = minZ; slice <= maxZ; slice++)
            {
                IcyBufferedImage image = seq.getImage(time, slice);
                image = applyRotation2D(image, c, angle, preserveImageSize);
                seq.setImage(time, slice, image);
            }
    }
    
    /**
     * @param img
     * @param channel
     *            the channel to rotate (or -1 for all)
     * @param angle
     * @return
     */
    public static IcyBufferedImage applyRotation2D(IcyBufferedImage img, int channel, double angle,
            boolean preserveImageSize)
    {
        if (angle == 0.0)
            return img;
        
        // start with the rotation to calculate the largest bounds
        IcyBufferedImage rotImg = IcyBufferedImageUtil.rotate(img.getImage(channel), angle);
        
        // calculate the difference in bounds
        Rectangle oldSize = img.getBounds();
        Rectangle newSize = rotImg.getBounds();
        int dw = (newSize.width - oldSize.width) / 2;
        int dh = (newSize.height - oldSize.height) / 2;
        
        if (channel == -1 || img.getSizeC() == 1)
        {
            if (preserveImageSize)
            {
                oldSize.translate(dw, dh);
                return IcyBufferedImageUtil.getSubImage(rotImg, oldSize);
            }
            return rotImg;
        }
        
        IcyBufferedImage[] newImages = new IcyBufferedImage[img.getSizeC()];
        
        if (preserveImageSize)
        {
            for (int c = 0; c < newImages.length; c++)
                if (c == channel)
                {
                    // crop the rotated channel
                    oldSize.translate(dw, dh);
                    newImages[c] = IcyBufferedImageUtil.getSubImage(rotImg, oldSize);
                }
                else
                    newImages[c] = img.getImage(c);
        }
        else
        {
            for (int c = 0; c < newImages.length; c++)
                if (c != channel)
                {
                    // enlarge and center the non-rotated channels
                    newImages[c] = new IcyBufferedImage(newSize.width, newSize.height, 1, img.getDataType_());
                    newImages[c].copyData(img.getImage(c), null, new Point(dw, dh));
                }
                else
                    newImages[channel] = rotImg;
        }
        
        return IcyBufferedImage.createFrom(Arrays.asList(newImages));
    }
    
    /**
     * Translates the specified sequence
     * 
     * @param seq
     *            the sequence to translate
     * @param t
     *            the frame to translate (or -1 for all)
     * @param z
     *            the frame to translate (or -1 for all)
     * @param c
     *            the frame to translate (or -1 for all)
     * @param preserveImageSize
     */
    public static void applyTranslation2D(Sequence seq, int t, int z, int c, Vector2d vector, boolean preserveImageSize)
    {
        if (vector.lengthSquared() == 0.0)
            return;
        
        int minT = (t == -1 ? 0 : t), maxT = (t == -1 ? seq.getSizeT() - 1 : t);
        int minZ = (z == -1 ? 0 : z), maxZ = (z == -1 ? seq.getSizeZ() - 1 : z);
        
        for (int time = minT; time <= maxT; time++)
            for (int slice = minZ; slice <= maxZ; slice++)
            {
                IcyBufferedImage image = seq.getImage(time, slice);
                image = applyTranslation2D(image, c, vector, preserveImageSize);
                seq.setImage(time, slice, image);
            }
    }
    
    public static IcyBufferedImage applyTranslation2D(IcyBufferedImage image, int channel, Vector2d vector,
            boolean preserveImageSize)
    {
        int dx = (int) Math.round(vector.x);
        int dy = (int) Math.round(vector.y);
        
        if (dx == 0 && dy == 0)
            return image;
        
        Rectangle newSize = image.getBounds();
        newSize.width += Math.abs(dx);
        newSize.height += Math.abs(dy);
        
        Point dstPoint_shiftedChannel = new Point(Math.max(0, dx), Math.max(0, dy));
        Point dstPoint_otherChannels = new Point(Math.max(0, -dx), Math.max(0, -dy));
        
        IcyBufferedImage newImage = new IcyBufferedImage(newSize.width, newSize.height, image.getSizeC(),
                image.getDataType_());
        for (int c = 0; c < image.getSizeC(); c++)
        {
            Point dstPoint = (channel == -1 || c == channel) ? dstPoint_shiftedChannel : dstPoint_otherChannels;
            newImage.copyData(image, null, dstPoint, c, c);
        }
        
        if (preserveImageSize)
        {
            newSize = image.getBounds();
            newSize.x = Math.max(0, -dx);
            newSize.y = Math.max(0, -dy);
            
            return IcyBufferedImageUtil.getSubImage(newImage, newSize);
        }
        return newImage;
    }
    
    /**
     * @param array
     * @param n
     *            limits the argMax to the first n elements of the input array
     * @return
     */
    private static int argMax(float[] array, int n)
    {
        int argMax = 0;
        float max = array[0];
        for (int i = 1; i < n; i++)
        {
            float val = array[i];
            if (val > max)
            {
                max = val;
                argMax = i;
            }
        }
        return argMax;
    }
    
    /**
     * Creates a Log-Polar view of the input image with default center (= image center) and
     * precision (360)
     * 
     * @return A Log-Polar view of the input image
     */
    private static IcyBufferedImage toLogPolar(IcyBufferedImage image)
    {
        return toLogPolar(image, image.getWidth() / 2, image.getHeight() / 2, 1080, 360);
    }
    
    /**
     * @param image
     * @param sizeTheta
     *            number of sectors
     * @param sizeRho
     *            number of rings
     * @return
     */
    private static IcyBufferedImage toLogPolar(IcyBufferedImage image, int centerX, int centerY, int sizeTheta,
            int sizeRho)
    {
        int sizeC = image.getSizeC();
        
        // create the log-polar image (X = theta, Y = rho)
        
        // theta: number of sectors
        double theta = 0.0, dtheta = 2 * Math.PI / sizeTheta;
        // pre-compute all sine/cosines
        float[] cosTheta = new float[sizeTheta];
        float[] sinTheta = new float[sizeTheta];
        for (int thetaIndex = 0; thetaIndex < sizeTheta; thetaIndex++, theta += dtheta)
        {
            cosTheta[thetaIndex] = (float) Math.cos(theta);
            sinTheta[thetaIndex] = (float) Math.sin(theta);
        }
        
        // rho: number of rings
        float drho = (float) (Math.sqrt(centerX * centerX + centerY * centerY) / sizeRho);
        
        IcyBufferedImage logPol = new IcyBufferedImage(sizeTheta, sizeRho, sizeC, DataType.FLOAT);
        
        for (int c = 0; c < sizeC; c++)
        {
            float[] out = logPol.getDataXYAsFloat(c);
            
            // first ring (rho=0): center value
            Array1DUtil.fill(out, 0, sizeTheta, getPixelValue(image, centerX, centerY, c));
            
            // Other rings
            float rho = drho;
            int outOffset = sizeTheta;
            for (int rhoIndex = 1; rhoIndex < sizeRho; rhoIndex++, rho += drho)
                for (int thetaIndex = 0; thetaIndex < sizeTheta; thetaIndex++, outOffset++)
                {
                    double x = centerX + rho * cosTheta[thetaIndex];
                    double y = centerY + rho * sinTheta[thetaIndex];
                    out[outOffset] = getPixelValue(image, x, y, c);
                }
        }
        
        logPol.updateChannelsBounds();
        return logPol;
    }
    
    /**
     * Calculates the 2D image value at the given real coordinates by bilinear interpolation
     * 
     * @param imageFloat
     *            the image to sample (must be of type {@link DataType#DOUBLE})
     * @param x
     *            the X-coordinate of the point
     * @param y
     *            the Y-coordinate of the point
     * @return the interpolated image value at the given coordinates
     */
    private static float getPixelValue(IcyBufferedImage img, double x, double y, int c)
    {
        int width = img.getWidth();
        int height = img.getHeight();
        Object data = img.getDataXY(c);
        DataType type = img.getDataType_();
        
        // "center" the coordinates to the center of the pixel
        x -= 0.5;
        y -= 0.5;
        
        int i = (int) Math.floor(x);
        int j = (int) Math.floor(y);
        
        if (i <= 0 || i >= width - 1 || j <= 0 || j >= height - 1)
            return 0f;
        
        float value = 0;
        
        final int offset = i + j * width;
        final int offset_plus_1 = offset + 1; // saves 1 addition
        
        x -= i;
        y -= j;
        
        final double mx = 1 - x;
        final double my = 1 - y;
        
        value += mx * my * Array1DUtil.getValueAsFloat(data, offset, type);
        value += x * my * Array1DUtil.getValueAsFloat(data, offset_plus_1, type);
        value += mx * y * Array1DUtil.getValueAsFloat(data, offset + width, type);
        value += x * y * Array1DUtil.getValueAsFloat(data, offset_plus_1 + width, type);
        
        return value;
    }
    
    /**
     * Apply a (forward) FFT on real data.
     * 
     * @param data
     *            the data to transform.
     * @param fft
     *            An FFT object to perform the transform
     * @return the complex, Fourier-transformed data.
     */
    private static float[] forwardFFT(float[] realData, FloatFFT_2D fft)
    {
        float[] out = new float[realData.length * 2];
        
        // format the input as a complex array
        // => real and imaginary values are interleaved
        for (int i = 0, j = 0; i < realData.length; i++, j += 2)
            out[j] = realData[i];
        
        fft.complexForward(out);
        return out;
    }
    
    /**
     * Apply an inverse FFT on complex data.
     * 
     * @param data
     *            the complex data to transform.
     * @param fft
     *            An FFT object to perform the transform
     * @return the real, Fourier-inverse data.
     */
    private static float[] inverseFFT(float[] cplxData, FloatFFT_2D fft)
    {
        float[] out = new float[cplxData.length / 2];
        
        fft.complexInverse(cplxData, true);
        
        // format the input as a real array
        // => skip imaginary values
        for (int i = 0, j = 0; i < cplxData.length; i += 2, j++)
            out[j] = cplxData[i];
        
        return out;
    }
    
    private static float[] spectralCorrelation(float[] a1, float[] a2, int width, int height)
    {
        // JTransforms's FFT takes dimensions as (rows, columns)
        FloatFFT_2D fft = new FloatFFT_2D(height, width);
        
        return spectralCorrelation(a1, a2, width, height, fft);
    }
    
    private static float[] spectralCorrelation(float[] a1, float[] a2, int width, int height, FloatFFT_2D fft)
    {
        // FFT on images
        float[] sourceFFT = forwardFFT(a1, fft);
        float[] targetFFT = forwardFFT(a2, fft);
        
        // Compute correlation
        
        Complex c1 = new Complex(), c2 = new Complex();
        for (int i = 0; i < sourceFFT.length; i += 2)
        {
            c1.setReal(sourceFFT[i]);
            c1.setImag(sourceFFT[i + 1]);
            
            c2.setReal(targetFFT[i]);
            c2.setImag(targetFFT[i + 1]);
            
            // correlate c1 and c2 (no need to normalize)
            c1.timesEquals(c2.conjugate());
            
            sourceFFT[i] = (float) c1.getReal();
            sourceFFT[i + 1] = (float) c1.getImag();
        }
        
        // IFFT
        
        return inverseFFT(sourceFFT, fft);
    }
}
